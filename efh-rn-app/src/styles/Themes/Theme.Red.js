import { extendTheme } from "native-base";

const ThemeRed = extendTheme({
  colors: {
    // Add new color
    primary: {
      50: "#E3F2F9",
      100: "#C5E4F3",
      200: "#A2D4EC",
      300: "#7AC1E4",
      400: "#47A9DA",
      500: "#BB0019", // main color
      600: "#007AB8",
      700: "#006BA1",
      800: "#005885",
      900: "#003F5E"
    }
  },
  components: {
    Button: {
      baseStyle: {
        rounded: "xl",
        color: "white"
      },
      defaultProps: {
        colorScheme: "primary"
      }
    }
  }
});

const colorThemeDark = {
  colors: {
    primary: {
      1: "black",
      2: "red",
      3: "brown"
    }
  }
};

export default ThemeRed;
