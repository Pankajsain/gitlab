export const ContentType = {
  TEXT: "TEXT",
  MEDIA: "MEDIA"
};

export const MediaType = {
  IMAGE: "IMAGE",
  AUDIO: "AUDIO",
  VIDEO: "VIDEO"
};

export const QuestionType = { ...ContentType };

export const AnswerType = {
  TEXT_PARAGRAPH: "TEXT_PARAGRAPH",
  TEXT_OPTIONS: "TEXT_OPTIONS",
  TEXT_MATCH_OPTIONS: "TEXT_MATCH_OPTIONS",
  ...MediaType
};
