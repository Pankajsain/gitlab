export const AuthNavigationScreens = {
  SignIn: {
    name: "AuthSigninScreen",
    title: "Sign In"
  },
  ContinueSession: {
    name: "ContinueSession",
    title: "Continue Session"
  },
  SignUp: {
    name: "AuthSignupScreen",
    title: "Sign Up"
  },
  ForgotPassword: {
    name: "AuthForgotPasswordScreen",
    title: "Forgot Password"
  }
};

export const MainNavigationScreens = {
  Dashboard: {
    name: "MainDashboardScreen",
    title: "Dashboard"
  },
  StartExam: {
    name: "ExamStackScreen",
    title: "Exam Screen"
  }
};

export const ExamNavigationScreens = {
  Unlock: {
    name: "ExamUnlockScreen",
    title: "Unlock Exam"
  },
  ExamStart: {
    name: "ExamStartScreen",
    title: "Exam Started"
  }
};
