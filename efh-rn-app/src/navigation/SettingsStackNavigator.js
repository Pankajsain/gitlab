import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

const SettingsStack = createStackNavigator();

const SettingsStackNavigator = () => {
  return <SettingsStack.Navigator></SettingsStack.Navigator>;
};

export default SettingsStackNavigator;
