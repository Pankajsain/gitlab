import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { ExamNavigationScreens } from "~/shared/constants/NavigationScreens";

import ExamUnlockScreen from "~/screens/Exam/ExamUnlockScreen";
import ExamStartScreen from "~/screens/Exam/ExamStartScreen";

const ExamStack = createStackNavigator();

const ExamStackNavigator = () => {
  return (
    <ExamStack.Navigator screenOptions={{ headerShown: false }}>
      <ExamStack.Screen name={ExamNavigationScreens.Unlock.name} component={ExamUnlockScreen} />
      <ExamStack.Screen name={ExamNavigationScreens.ExamStart.name} component={ExamStartScreen} />
    </ExamStack.Navigator>
  );
};

export default ExamStackNavigator;
