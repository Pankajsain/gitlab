import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

const TestPermissionStack = createStackNavigator();

const TestPermissionStackNavigator = () => {
  return <TestPermissionStack.Navigator></TestPermissionStack.Navigator>;
};

export default TestPermissionStackNavigator;
