import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { persistReducer, FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER } from "redux-persist";
import AsyncStorage from "@react-native-async-storage/async-storage";
import reducer from "./slices/";
import timerMiddleware from "./middlewares/Timer.middleware";

const defaultMiddlewares = getDefaultMiddleware({
  serializableCheck: {
    ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER] //ignore redux-persist actions
  }
}); // thunk, immutableCheck and serializableCheck
const appMiddlewares = [
  // timerMiddleware
]; // Custom middlewares
const middleware = [...defaultMiddlewares, ...appMiddlewares]; // Combine middlewares

const persistConfig = { key: "root", storage: AsyncStorage };
const persistedReducer = persistReducer(persistConfig, reducer);

// Create store
const store = configureStore({
  reducer: persistedReducer,
  middleware
});

export default store;
