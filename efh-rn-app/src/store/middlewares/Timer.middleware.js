const addZero = (num, places) => String(num).padStart(places, "0");

const currentDateTime = () => {
  var d = new Date();
  var h = addZero(d.getHours(), 2);
  var m = addZero(d.getMinutes(), 2);
  var s = addZero(d.getSeconds(), 2);
  var ms = addZero(d.getMilliseconds(), 3);
  return h + ":" + m + ":" + s + ":" + ms;
};

// Test redux middleware for logging execution time of an dispatched action
const timerMiddleware =
  ({ dispatch, getStore }) =>
  (next) =>
  (action) => {
    console.info("timerMiddleware - start", action, currentDateTime());
    next(action);
    console.info("timerMiddleware - end", action, currentDateTime());
  };

export default timerMiddleware;
