import { createSlice } from "@reduxjs/toolkit";
import { AnswerType } from "~/shared/constants/Types";

const initialState = {
  questionPaper: null,
  startTime: null,
  endTime: null
};

const getSection = (state, questionId) => {
  return state.questionPaper.sections.find(
    (s) => s.questions.findIndex((q) => q.id === questionId) >= 0
  );
};

const ExamSessionSlice = createSlice({
  name: "ExamSession",
  initialState,
  reducers: {
    loadQuestionPaper: (state, action) => {
      state.questionPaper = action.payload;
    },
    selectSingleAnswerOption: (state, action) => {
      const { questionId, answerId } = action.payload;

      const section = getSection(state, questionId);
      if (section) {
        const question = section.questions.find((q) => q.id === questionId);

        if (
          question.answer.contentType === AnswerType.TEXT_OPTIONS &&
          !question.answer.content.isMultiSelect
        ) {
          question.answer.content.data = question.answer.content.data.map((ans) => {
            ans.selected = ans.id === answerId;
            return ans;
          });
        }
      }
    },
    markQuestionForLater: (state, action) => {
      const { questionId, status } = action.payload;

      const section = getSection(state, questionId);
      if (section) {
        const question = section.questions.find((q) => q.id === questionId);
        question.markForLater = !!status;
      }
    },
    updateTextAnswer: (state, action) => {
      const { questionId, data } = action.payload;

      const section = getSection(state, questionId);

      if (section) {
        const question = section.questions.find((q) => q.id === questionId);

        if (question.answer.contentType === AnswerType.TEXT_PARAGRAPH) {
          question.answer.content.data = data;
        }
      }
    }
  }
});

export const {
  loadQuestionPaper,
  selectSingleAnswerOption,
  markQuestionForLater,
  updateTextAnswer
} = ExamSessionSlice.actions;

export default ExamSessionSlice.reducer;
