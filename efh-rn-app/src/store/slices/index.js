import { combineReducers } from "@reduxjs/toolkit";
import user from "./User.slice";
import examSession from "./ExamSession.slice";

// Root reduce / combined reducers for the store
const reducer = combineReducers({
  user,
  examSession
});

export default reducer;
