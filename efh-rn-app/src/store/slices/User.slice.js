import { createSlice } from "@reduxjs/toolkit";

// Initial User State
const initialState = {
  userId: null,
  userData: null,
  loggedIn: false,
  sessionVerified: false,
  userList: [],
  activeUserID: ""
};

// User redux state implementation
const UserSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    signin: (state, action) => {
      let userList = [...state.userList];
      let userIndex = userList.findIndex((user) => user.userId === action.payload.userId);
      if (userIndex < 0) {
        userList.push({
          userInfo: action.payload,
          sessionInfo: {
            lastLoginTime: new Date().getTime()
          }
        });
      } else {
        userList[userIndex].sessionInfo.lastLoginTime = new Date().getTime();
      }
      state.userList = userList;
      state.userId = action.payload.userId;
      state.userData = action.payload;
      state.loggedIn = true;
    },
    signout: (state, action) => {
      state.loggedIn = false;
    },
    reset: (state, action) => {
      state.sessionVerified = action.payload;
    }
  }
});

// Exporting all users action creaters
export const { signin, signout, reset } = UserSlice.actions;

// Exprting user redux reducer
export default UserSlice.reducer;
