import React, { useEffect } from "react";
import { StyleSheet, Platform, View } from "react-native";
import { NativeBaseProvider } from "native-base";
import { useSelector } from "react-redux";

import AuthStackNavigator from "./navigation/AuthStackNavigator";
import ExamStackNavigator from "./navigation/ExamStackNavigator";
import MainStackNavigator from "./navigation/MainStackNavigator";

import useAuth from "~/helpers/hooks/useAuth";
import ThemeRed from "./styles/Themes/Theme.Red";

export default function App() {
  const isLoggedIn = useSelector((state) => state.user.loggedIn);
  const isSessionValid = useSelector((state) => state.user.sessionVerified);

  console.log(ThemeRed);

  const { resetSession } = useAuth();
  useEffect(() => {
    resetSession();
  }, []);

  console.log(Platform.OS);

  if (Platform.OS == "web") {
    if (navigator.userAgent.toLowerCase().indexOf("electron/") > -1) {
      console.log(window.process);
      const { ipcRenderer } = window.require("electron");

      // Synchronous message emmiter and handler
      // ipcRenderer.sendSync('synchronous-message', 'sync ping')

      //Async message sender
      ipcRenderer.send("asynchronous-message", "async ping");
    }
  }

  return (
    <NativeBaseProvider theme={ThemeRed}>
      <View style={styles.container}>
        {/* <ExamStackNavigator /> */}
        {isLoggedIn && isSessionValid ? <MainStackNavigator /> : <AuthStackNavigator />}
        {/* {isLoggedIn ? <MainStackNavigator /> : <ExamStackNavigator />} */}
      </View>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
