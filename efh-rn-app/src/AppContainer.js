import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import React, { Component } from "react";
import { View } from "react-native";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";
import store from "./store";
import App from "./App";

import * as Font from "expo-font";

const persistor = persistStore(store);

export default class AppContainer extends Component {
  state = {
    loading: false
  };

  //   async componentDidMount() {
  //     await Font.loadAsync({
  //       Roboto: require("native-base/Fonts/Roboto.ttf"),
  //       Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
  //     });
  //     this.setState({ loading: true });
  //   }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <NavigationContainer theme={{ colors: { background: "transparent" } }}>
            {this.state.loading ? <View /> : <App />}
          </NavigationContainer>
        </PersistGate>
      </Provider>
    );
  }
}
