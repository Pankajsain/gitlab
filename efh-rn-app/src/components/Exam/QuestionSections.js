import React from "react";
import { Text } from "native-base";
import styled from "styled-components/native";

import UIView from "../UI/Layout/UIView";

const RootContainer = styled(UIView)`
  border-bottom-width: 1;
  border-bottom-color: lightgray;
`;

const QuestionSections = () => {
  return (
    <RootContainer justifyContent="center" width="100%" padding="10px">
      <Text>Sections</Text>
    </RootContainer>
  );
};

export default QuestionSections;
