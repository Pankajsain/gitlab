import React, { useEffect, useState } from "react";
import useExamSession from "~/helpers/hooks/useExamSession";
import styled from "styled-components/native";
import UIView from "../UI/Layout/UIView";
import { Text } from "native-base";

const RootContainer = styled(UIView)`
  border-bottom-width: 2px;
  border-bottom-color: lightgray;
`;

const HeaderContainer = styled.View`
  padding-left: 10px;
  flex-grow: 1;
`;

const Title = styled.Text`
  font-size: 24px;
  font-weight: bold;
`;

const SubTitle = styled.Text`
  font-size: 12px;
  font-weight: 500;
  color: gray;
`;

const Marks = styled.Text`
  font-size: 20px;
  font-weight: bold;
  flex-grow: 1;
`;

const Clock = styled.Text`
  font-size: 24px;
  font-weight: bold;
`;

const QuestionPaperHeader = () => {
  const { questionPaperDetails } = useExamSession();

  const [timeRemained, setTimeRemained] = useState("");

  useEffect(() => startClock(), [questionPaperDetails]);

  const startClock = () => {
    if (!questionPaperDetails) return;

    const {
      schedule: { startTime, endTime }
    } = questionPaperDetails;

    let dateDiff = new Date(endTime) - new Date(startTime);

    const intervalId = setInterval(() => {
      if (dateDiff > 0) {
        dateDiff = dateDiff - 1000;

        const seconds = ("0" + Math.floor((dateDiff / 1000) % 60)).slice(-2);
        const minutes = ("0" + Math.floor((dateDiff / (1000 * 60)) % 60)).slice(-2);
        const hours = ("0" + Math.floor((dateDiff / (1000 * 60 * 60)) % 24)).slice(-2);

        const time = `${hours}:${minutes}:${seconds}`;

        setTimeRemained(time);
      }
    }, 1000);

    return () => {
      clearInterval(intervalId);
    };
  };

  return (
    <RootContainer padding="5px 10px" width="100%">
      {!questionPaperDetails && <Text>Loading...</Text>}
      {questionPaperDetails && (
        <>
          <HeaderContainer>
            <Title>{questionPaperDetails.name}</Title>
            <SubTitle>{questionPaperDetails.center}</SubTitle>
          </HeaderContainer>
          <Marks>Marks - {questionPaperDetails.marks}</Marks>
          <Clock>{timeRemained}</Clock>
        </>
      )}
    </RootContainer>
  );
};

export default QuestionPaperHeader;
