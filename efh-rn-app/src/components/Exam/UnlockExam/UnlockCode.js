import { Text } from "native-base";
import React from "react";
import { View } from "react-native";
import styled from "styled-components/native";
import Spacer from "~/components/UI/Common/Spacer";

import UIPinCode from "~/components/UI/Inputs/UIPinCode";

const Container = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Title = styled.Text`
  font-size: 16px;
  font-weight: 700;
  max-width: 200px;
  text-align: center;
`;

const UnlockCode = () => {
  const onChangeHandler = (value) => {
    console.log("pin", value);
  };
  return (
    <Container>
      <Title>Please enter unlock key to start the exam</Title>
      <Text style={{ fontSize: 11, marginTop: 5 }}>
        Unlocking of the exam will start the audio & video recording.
      </Text>
      <Spacer />
      <UIPinCode length={4} onChange={onChangeHandler} />
    </Container>
  );
};

export default UnlockCode;
