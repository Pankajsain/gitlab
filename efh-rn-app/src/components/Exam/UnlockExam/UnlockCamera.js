import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Camera } from "expo-camera";
import { Icon } from "native-base";
import { Ionicons } from "@expo/vector-icons";

const UnlockCamera = () => {
  const [hasPermission, setHasPermission] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.front);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, []);

  if (hasPermission === null) {
    return <View />;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View style={styles.container}>
      <Camera style={styles.camera} type={type}></Camera>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            console.log("flip pressed");
            setType(
              type === Camera.Constants.Type.back
                ? Camera.Constants.Type.front
                : Camera.Constants.Type.back
            );
          }}
        >
          <Icon style={styles.flipBtn} as={<Ionicons name="camera-reverse-outline" />} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 300,
    height: 200,
    backgroundColor: "#f1f1f1",
    position: "relative"
  },
  camera: {
    width: "100%",
    height: 200,
    position: "absolute"
  },
  flipBtn: {},
  buttonContainer: {
    flex: 1,
    flexDirection: "column",
    position: "absolute",
    height: "100%",
    width: "25%",
    left: "100%",
    justifyContent: "flex-end",
    alignContent: "center",
    zIndex: 2
  },
  button: {
    alignItems: "center",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: 40
  }
});

export default UnlockCamera;
