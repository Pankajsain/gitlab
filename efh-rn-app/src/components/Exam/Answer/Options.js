import React from "react";
import { View, Text } from "react-native";
import Spacer from "~/components/UI/Common/Spacer";
import UIRadioButton from "~/components/UI/Inputs/UIRadioButton";

const Options = ({ data, onChange }) => {
  const { isMultiSelect, data: optionsList } = data;

  const clickHandler = (id) => () => {
    if (onChange) onChange(id);
  };

  return (
    <View>
      {optionsList &&
        optionsList.map((option) => (
          <View key={option.id}>
            <UIRadioButton
              checked={option.selected}
              label={option.title}
              onClick={clickHandler(option.id)}
            />
            <Spacer />
          </View>
        ))}
    </View>
  );
};

export default Options;
