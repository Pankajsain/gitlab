import { TextArea } from "native-base";
import React from "react";

import UIFlex from "~/components/UI/Layout/UIFlex";

const Paragraph = ({ onChange }) => {
  const textChangeHandler = (e) => {
    if (!e) return;
    e.preventDefault();

    const value = e.currentTarget.value;

    if (onChange) onChange(value);
  };

  return (
    <UIFlex>
      <TextArea
        style={{ width: "100%", borderRadius: 10 }}
        onBlur={textChangeHandler}
        rowSpan={5}
        bordered
      />
    </UIFlex>
  );
};

export default React.memo(Paragraph);
