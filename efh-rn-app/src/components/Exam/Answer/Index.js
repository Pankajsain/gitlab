import React from "react";
import { View, Text } from "react-native";

import { AnswerType } from "~/shared/constants/Types";

import UIFlex from "~/components/UI/Layout/UIFlex";

import Options from "./Options";
import Paragraph from "./Paragraph";

const Answer = ({ data, textAnswerChange, optionSelectionChange }) => {
  const { contentType, content } = data;

  const isOptions = contentType === AnswerType.TEXT_OPTIONS;
  const isParagraph = contentType === AnswerType.TEXT_PARAGRAPH;

  return (
    <UIFlex paddingL="50px" paddingR="50px">
      {isOptions && <Options data={content} onChange={optionSelectionChange} />}
      {isParagraph && <Paragraph data={content} onChange={textAnswerChange} />}
    </UIFlex>
  );
};

export default React.memo(Answer);
