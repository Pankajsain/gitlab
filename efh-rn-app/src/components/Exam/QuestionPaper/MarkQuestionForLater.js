import React from "react";
import { Icon, Text } from "native-base";
import { TouchableOpacity } from "react-native";

import UIFlex from "~/components/UI/Layout/UIFlex";
import Spacer from "~/components/UI/Common/Spacer";

const MarkQuestionForLater = ({ checked, onChange }) => {
  const onPressHandler = () => {
    if (onChange) onChange(!checked);
  };

  return (
    <TouchableOpacity onPress={onPressHandler}>
      <UIFlex direction="row" alignItems="center">
        <Icon type="Feather" name="flag" style={{ color: checked ? "red" : "black" }} />
        <Spacer />
        <Text style={{ fontWeight: "bold", color: "gray" }}>Mark for later</Text>
      </UIFlex>
    </TouchableOpacity>
  );
};

export default MarkQuestionForLater;
