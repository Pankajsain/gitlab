import React, { useEffect, useState } from "react";
import styled from "styled-components/native";
import Spacer from "~/components/UI/Common/Spacer";

import UIView from "~/components/UI/Layout/UIView";
import useExamSession from "~/helpers/hooks/useExamSession";

import Question from "./Question";

const RootContainer = styled(UIView)`
  position: absolute;
  top: 0px;
  bottom: 0px;
`;

const QuestionPaper = () => {
  const { questionPaper: data } = useExamSession();
  const [questions, setQuestions] = useState([]);

  useEffect(() => {
    loadQuestions();
  }, [data]);

  const loadQuestions = () => {
    if (!data) return;

    let list = [];
    data.sections.forEach((m) => {
      list = [...list, ...m.questions];
    });

    setQuestions(list);
  };

  return (
    <RootContainer direction="column" padding="10px" width="100%">
      {questions.map((question, i) => (
        <Question key={question.id} data={question} index={i} />
      ))}
      <Spacer />
    </RootContainer>
  );
};

export default React.memo(QuestionPaper);
