import React, { useCallback } from "react";
import { View } from "react-native";
import { Badge, Card, CardItem, Text, Box } from "native-base";
import styled from "styled-components/native";

import UIFlex from "~/components/UI/Layout/UIFlex";
import UIHeader from "~/components/UI/Text/UIHeader";
import Spacer from "~/components/UI/Common/Spacer";

import { QuestionType } from "~/shared/constants/Types";
import QuestionMedia from "./QuestionMedia";
import MarkQuestionForLater from "./MarkQuestionForLater";
import useExamSession from "~/helpers/hooks/useExamSession";
import UIView from "~/components/UI/Layout/UIView";

const RootCotainer = styled((props) => <View {...props} />)`
  width: 100%;
`;

const Question = ({ data, index }) => {
  const {
    id: questionId,
    marks,
    markForLater,
    contentType,
    answer,
    content: { title, subTitle, ...mediaProps }
  } = data;

  const { changeMarkForLaterStatus, changeAnswerOption, changeAnswerText } = useExamSession();

  const isMedia = contentType === QuestionType.MEDIA;

  const markForLaterClickHandler = useCallback((status) => {
    changeMarkForLaterStatus(questionId, status);
  }, []);

  const answerTextChangeHandler = useCallback((value) => {
    changeAnswerText(questionId, value);
  });

  const answerOptionChangeHandler = useCallback((optionId) => {
    changeAnswerOption(questionId, optionId);
  });

  return (
    <RootCotainer>
      <Box>
        <UIView direction="column" width="100%">
          <UIView direction="column" width="100%">
            <UIView width="100%">
              <Badge style={{ backgroundColor: "gray" }}>
                <Text style={{ color: "white" }}>{index + 1}</Text>
              </Badge>
              <Spacer />
              <UIView flexGrow={1} justifyContent="space-between">
                <UIHeader title={title} style={{ width: "auto" }} />
                <UIView>
                  <MarkQuestionForLater
                    checked={markForLater}
                    onChange={markForLaterClickHandler}
                  />
                  <Spacer size={10} />
                  <View>
                    <Text>{marks} Marks</Text>
                  </View>
                </UIView>
              </UIView>
            </UIView>
            <Spacer />

            <Spacer size={10} />
            <UIFlex paddingL="20px">{isMedia && <QuestionMedia data={mediaProps} />}</UIFlex>
          </UIView>
          <Spacer size={10} />
          <Answer
            data={answer}
            optionSelectionChange={answerOptionChangeHandler}
            textAnswerChange={answerTextChangeHandler}
          />
        </UIView>
      </Box>
    </RootCotainer>
  );
};

export default React.memo(Question);
