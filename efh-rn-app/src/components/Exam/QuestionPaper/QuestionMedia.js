import React from "react";
import { Text } from "native-base";
import styled from "styled-components/native";

import { MediaType } from "~/shared/constants/Types";

const MediaView = styled.View`
  height: 200px;
  width: 200px;
  background-color: gray;
`;

const QuestionMedia = ({ data }) => {
  const isImage = data.type === MediaType.IMAGE;
  const isVideo = data.type === MediaType.VIDEO;
  const isAudio = data.type === MediaType.AUDIO;

  return (
    <MediaView>
      <Text>
        {isImage && "Image"}
        {isVideo && "Video"}
        {isAudio && "Audio"}
      </Text>
    </MediaView>
  );
};

export default QuestionMedia;
