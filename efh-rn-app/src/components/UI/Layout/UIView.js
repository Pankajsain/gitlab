import React from "react";
import styled from "styled-components/native";

const RootContainer = styled.View`
  position: relative;
  display: flex;
  flex-direction: ${({ direction }) => direction};

  flex-grow: ${({ flexGrow }) => flexGrow};
  flex-shrink: ${({ flexShrink }) => flexShrink};
  flex-basis: ${({ flexBasis }) => flexBasis};
  flex-wrap: ${({ flexWrap }) => flexWrap};

  justify-content: ${({ justifyContent }) => justifyContent};
  align-items: ${({ alignItems }) => alignItems};
  align-content: ${({ alignContent }) => alignContent};

  height: ${({ height }) => height};
  width: ${({ width }) => width};

  padding-top: ${({ padding }) => padding.t};
  padding-bottom: ${({ padding }) => padding.b};
  padding-right: ${({ padding }) => padding.r};
  padding-left: ${({ padding }) => padding.l};

  margin-top: ${({ margin }) => margin.t};
  margin-bottom: ${({ margin }) => margin.b};
  margin-right: ${({ margin }) => margin.r};
  margin-left: ${({ margin }) => margin.l};
`;

const UIView = ({
  children,
  style,
  direction,
  flexGrow,
  flexShrink,
  flexBasis,
  flexWrap,
  justifyContent,
  alignItems,
  alignContent,
  height,
  width,
  padding,
  margin
}) => {
  direction = direction || "row";

  flexGrow = flexGrow || 0;
  flexShrink = flexShrink || 1;
  flexBasis = flexBasis || "auto";
  flexWrap = flexWrap || "nowrap";

  justifyContent = justifyContent || "flex-start";
  alignItems = alignItems || "center";
  alignContent = alignContent || "normal";

  height = height || "auto";
  width = width || "auto";

  padding = getPaddingMargin(padding);
  margin = getPaddingMargin(margin);

  return (
    <RootContainer
      {...{
        style,
        direction,
        flexGrow,
        flexShrink,
        flexBasis,
        flexWrap,
        justifyContent,
        alignItems,
        alignContent,
        height,
        width,
        padding,
        margin
      }}
    >
      {children}
    </RootContainer>
  );
};

const getPaddingMargin = (data) => {
  const arrayData = (data || "").split(" ");

  if (arrayData.length < 1 || arrayData.length > 4)
    return { t: "0px", b: "0px", r: "0px", l: "0px" };

  if (arrayData.length === 1)
    return {
      t: arrayData[0],
      b: arrayData[0],
      r: arrayData[0],
      l: arrayData[0]
    };

  if (arrayData.length === 2)
    return {
      t: arrayData[0],
      b: arrayData[0],
      r: arrayData[1],
      l: arrayData[1]
    };

  if (arrayData.length === 4)
    return {
      t: arrayData[0],
      r: arrayData[1],
      b: arrayData[2],
      l: arrayData[3]
    };
};

export default UIView;
