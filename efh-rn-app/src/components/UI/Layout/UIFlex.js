import React from "react";
import styled from "styled-components/native";

const RootContainer = styled.View`
  flex-grow: ${({ flexGrow }) => flexGrow};
  flex-shrink: ${({ flexShrink }) => flexShrink};
  flex-basis: ${({ flexBasis }) => flexBasis};
  flex-wrap: ${({ flexWrap }) => flexWrap};
  display: flex;
  flex-direction: ${({ direction }) => direction};
  justify-content: ${({ justifyContent }) => justifyContent};
  align-items: ${({ alignItems }) => alignItems};
  align-content: ${({ alignContent }) => alignContent};
  background-color: ${({ bgColor }) => bgColor};

  padding: ${({ padding }) => padding};

  padding-left: ${({ paddingL }) => paddingL};
  padding-right: ${({ paddingR }) => paddingR};

  padding-top: ${({ paddingT }) => paddingT};
  padding-bottom: ${({ paddingB }) => paddingB};

  margin: ${({ margin }) => margin};
  position: ${({ position }) => position};
  height: ${({ height }) => height};
  width: ${({ width }) => width};
`;

const UIFlex = ({
  flexGrow,
  flexShrink,
  fleWrap,
  flexBasis,
  direction,
  justifyContent,
  alignItems,
  alignContent,
  bgColor,
  children,
  position,
  height,
  width,
  padding,
  margin,
  paddingL,
  paddingR,
  paddingT,
  paddingB,
  style
}) => {
  flexGrow = flexGrow || 0;
  flexShrink = flexShrink || 1;
  flexBasis = flexBasis || "auto";
  fleWrap = fleWrap || "nowrap";
  direction = direction || "column";
  justifyContent = justifyContent || "flex-start";
  alignItems = alignItems || "flex-start";
  alignContent = alignContent || "normal";
  bgColor = bgColor || "white";
  position = position || "relative";
  height = height || "auto";
  width = width || "100%";
  padding = padding || "0px";
  margin = margin || "0px";
  paddingL = paddingL || "0px";
  paddingR = paddingR || "0px";
  paddingT = paddingT || "0px";
  paddingB = paddingB || "0px";

  return (
    <RootContainer
      {...{
        flexGrow,
        flexShrink,
        flexBasis,
        fleWrap,
        direction,
        children,
        justifyContent,
        alignItems,
        alignContent,
        bgColor,
        style,
        position,
        height,
        width,
        padding,
        paddingL,
        paddingR,
        paddingT,
        paddingB,
        margin
      }}
    >
      {children}
    </RootContainer>
  );
};

export default UIFlex;
