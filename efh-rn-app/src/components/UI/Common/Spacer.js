import React from "react";
import { View, Text } from "react-native";

const Spacer = ({ size }) => {
  return <View style={{ flex: 0, padding: size || 5 }} />;
};

export default Spacer;
