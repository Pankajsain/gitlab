import React from "react";
import styled from "styled-components/native";
import Spacer from "~/components/UI/Common/Spacer";

const HeaderRoot = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`;

const ContentHeader = styled.Text`
  display: flex;
  flex-direction: column;
  align-items: center;
  font-size: ${({ size }) => size}px;
  font-weight: ${({ fontWeight }) => fontWeight};
  color: ${({ color }) => color};
`;

const ContentBody = styled.View`
  max-width: 80%;
  text-align: center;
`;
const Content = styled.Text`
  font-size: ${({ size }) => size}px;
  color: ${({ color }) => color};
  text-align: ${({ textAlign }) => textAlign};
`;

const UIHeader = ({
  children,
  title,
  fontSize,
  color,
  fontWeight,
  sfontSize,
  spacerSize,
  scolor,
  textAlign,
  style
}) => {
  spacerSize = spacerSize || 2;
  fontSize = fontSize || 18;
  color = color || "black";
  fontWeight = fontWeight || 500;
  textAlign = textAlign || "center";

  sfontSize = sfontSize || 14;
  scolor = scolor || "gray";

  style = style || {};

  return (
    <HeaderRoot style={style}>
      <ContentHeader size={fontSize} color={color} fontWeight={fontWeight} textAlign={textAlign}>
        {title}
      </ContentHeader>
      {children && (
        <>
          <Spacer size={spacerSize} />
          <ContentBody>
            <Content size={sfontSize} color={scolor}>
              {children}
            </Content>
          </ContentBody>
        </>
      )}
    </HeaderRoot>
  );
};

export default UIHeader;
