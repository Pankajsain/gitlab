import React from "react";
import { Icon, Text } from "native-base";
import styled from "styled-components/native";

import Spacer from "../Common/Spacer";
import { TouchableOpacity } from "react-native";

const RootContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 5px;
  margin-bottom: 5px;
`;

const UIRadioButton = ({ checked, label, style, onClick }) => {
  const name = checked ? "radio-btn-active" : "radio-btn-passive";
  return (
    <TouchableOpacity onPress={onClick}>
      <RootContainer style={style}>
        <Icon type="Fontisto" name={name} />
        <Spacer />
        <Text>{label}</Text>
      </RootContainer>
    </TouchableOpacity>
  );
};

export default UIRadioButton;
