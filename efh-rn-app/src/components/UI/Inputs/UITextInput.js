import React, { forwardRef } from "react";
import { Input, Text, FormControl } from "native-base";
import { StyleSheet, View } from "react-native";

const UITextInput = (
  { value, label, onChange, style, isPassword, placeholder, onBlur, error, disabled },
  ref
) => {
  const onChangeHandler = (value) => {
    //if (e) e.preventDefault();
    //const value = e.target.value;
    //console.log(value);
    onChange(value);
  };

  return (
    <View style={style}>
      <FormControl.Label>{label}</FormControl.Label>
      {/* <Item
        ref={ref}
        regular={true}
        style={[styles.root, !!disabled ? styles.disabled : {}]}
        error={!!error}
        bordered
        disabled={!!disabled}
      > */}
      <Input
        secureTextEntry={isPassword}
        //onChange={onChangeHandler}
        defaultValue={value}
        placeholder={placeholder}
        onBlur={onBlur}
        onChangeText={onChangeHandler}
        disabled={!!disabled}
        style={!!disabled ? styles.disabledInput : {}}
      />
      {/* </Item> */}
      {error && (
        <View>
          <Text style={styles.invalid}>{error.message}</Text>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  root: { marginTop: 5, borderRadius: 5 },
  invalid: {
    color: "red"
  },
  disabled: {
    backgroundColor: "whitesmoke",
    color: "#ccc"
  },
  disabledInput: {
    color: "#cccccc",
    backgroundColor: "whitesmoke"
  }
});

export default forwardRef(UITextInput);
