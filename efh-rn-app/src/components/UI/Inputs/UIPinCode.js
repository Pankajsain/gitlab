import React, { useCallback, useEffect, useRef, useState } from "react";
import { Input } from "native-base";
import styled from "styled-components/native";

const RootContainer = styled.View`
  text-align: center;
  display: flex;
  flex-direction: row;
  padding: 5px;
  margin-top: 10px;
  width: 100%;
  justify-content: center;
`;

const PinInput = styled(Input)`
  max-height: 40px;
  min-height: 40px;
  max-width: 40px;
  min-width: 40px;
  border-width: 1px;
  border-color: gray;
  margin-left: 5px;
  margin-right: 5px;
  text-align: center;
`;

const UIPinCode = ({ length, onChange }) => {
  if (length < 1) return <></>;

  const [pin, setPin] = useState([]);
  const inputs = useRef([]);

  useEffect(() => {
    if (inputs.current.length > 0) {
      inputs.current[0]._root.focus();
    }
  }, []);

  useEffect(() => {
    inputs.current = [];
    setPin([...Array(length)].map((i) => ""));
  }, [length]);

  useEffect(() => {
    onChangeCallback();
  }, [pin]);

  const addRef = useCallback((el) => {
    if (!el) return;
    inputs.current = [...inputs.current, el];
  });

  const onChangeCallback = () => {
    if (onChange) {
      const pinValue = pin.join("");
      if (pinValue.length === length) onChange(pinValue);
    }
  };

  const onChangeHandler = (index) => (e) => {
    if (!e) return;
    e.preventDefault();

    const value = e.currentTarget.value;

    setPin((prev) =>
      prev.map((p, i) => {
        if (i === index) return value;
        return p;
      })
    );

    if (value === "" && index > 0) {
      inputs.current[index - 1]._root.focus();
    } else if (index >= 0 && index < length) {
      inputs.current[index + 1]._root.focus();
    }
  };

  return (
    <RootContainer>
      {pin.map((p, i) => (
        <PinInput
          keyboardType="numeric"
          maxLength={1}
          key={i}
          ref={addRef}
          value={p}
          onChange={onChangeHandler(i)}
        />
      ))}
    </RootContainer>
  );
};

export default React.memo(UIPinCode);
