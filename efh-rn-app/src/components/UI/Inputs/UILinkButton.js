import { Text } from "native-base";
import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";

const UILinkButton = ({ children, style, onClick, ...rest }) => {
  return (
    <TouchableOpacity style={{ ...style, ...styles.root }} onPress={onClick} {...rest}>
      <Text>{children}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  root: {}
});

export default UILinkButton;
