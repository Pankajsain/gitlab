import React from "react";
import { Button, Text } from "native-base";
import { StyleSheet } from "react-native";

const UIButton = ({ children, style, onClick, full, ...rest }) => {
  const rootStyles = (() => {
    const fullWidth = { flexGrow: full ? 1 : 0 };
    return StyleSheet.flatten([styles.root, fullWidth, style]);
  })();

  return (
    <Button style={rootStyles} onPress={onClick} {...rest}>
      <Text style={{ color: "white" }}>{children}</Text>
    </Button>
  );
};

const styles = StyleSheet.create({
  root: { display: "flex", justifyContent: "center" }
});

export default UIButton;
