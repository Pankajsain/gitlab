import React, { useEffect, useState } from "react";
import { Checkbox, Text } from "native-base";
import { StyleSheet, TouchableOpacity } from "react-native";

const UICheckBox = ({ label, value, onChange }) => {
  const [checked, setChecked] = useState(value);
  const toggleHandler = () => {
    setChecked(!checked);
    if (onChange) onChange(!checked);
  };

  useEffect(() => {}, []);

  return (
    <TouchableOpacity style={styles.container} onPress={toggleHandler}>
      <Checkbox style={styles.checkbox} isChecked={checked} onPress={toggleHandler}>
        <Text style={styles.label}>{label}</Text>
      </Checkbox>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  checkbox: {
    margin: 0
  },
  label: {
    marginLeft: 20
  }
});

export default UICheckBox;
