import React from "react";
import styled from "styled-components/native";

const RootContainer = styled.View`
  display: flex;
  flex-direction: column;
  background-color: gray;
  min-height: ${({ height }) => height}px;
  height: ${({ height }) => height}px;
  min-width: ${({ width }) => width}px;
  width: ${({ width }) => width}px;
`;

const UICamera = ({ height, width }) => {
  height = height || "200";
  width = width || "200";

  return <RootContainer height={height} width={width} />;
};

export default UICamera;
