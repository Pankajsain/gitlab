import React from "react";
import { Text } from "react-native";
import { HStack, Left, Right, Icon, ListItem } from "native-base";

const ListItemData = (props) => {
  return (
    <HStack>
      <Text
        style={{
          paddingHorizontal: 15,
          paddingVertical: 12,
          height: 43,
          color: "#fff",
          textAlign: "center",
          backgroundColor: "#68a0cf",
          borderRadius: 100
        }}
      >
        {props.classLogo}
      </Text>
      <Text>{props.className}</Text>
      <Text style={{ fontSize: 13, color: "#8e7e7e" }} note>
        {props.classSyllabus}
      </Text>
      <Icon name="ellipsis-horizontal-outline" style={{}} />
    </HStack>
  );
};

export default ListItemData;
