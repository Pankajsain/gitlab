import React from "react";
import { HStack, Text } from "native-base";

const HeadingWithCount = (props) => {
  return (
    <HStack alignItems="center" space={5}>
      <Text>{props.title.toUpperCase()}</Text>
      <Text>{props.count}</Text>
    </HStack>
  );
};

export default HeadingWithCount;
