import React from "react";
import { StyleSheet, Image, TouchableOpacity, Text } from "react-native";
import { Icon, VStack } from "native-base";
import { Entypo, Ionicons } from "@expo/vector-icons";

const IconButton = (data) => {
  console.log(data);
  return (
    <TouchableOpacity>
      <Icon style={styles.icon} as={data.icon} />
    </TouchableOpacity>
  );
};

const Sidebar = () => {
  return (
    <VStack style={styles.container} alignItems="center">
      <VStack alignItems="center">
        <IconButton icon={<Entypo name="home" />} />
        <IconButton icon={<Ionicons name="book-outline" />} />
        <IconButton icon={<Ionicons name="ribbon-outline" />} />
        <IconButton icon={<Ionicons name="book-outline" />} />
        <IconButton icon={<Ionicons name="calendar-outline" />} />
        <IconButton icon={<Ionicons name="tv-outline" />} />
        <IconButton icon={<Ionicons name="grid-outline" />} />
      </VStack>
    </VStack>
  );
};

export default Sidebar;

const styles = StyleSheet.create({
  container: {
    width: 70,
    backgroundColor: "#BB0019"
  },
  icon: {
    color: "#fff",
    fontSize: 24,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: 60,
    width: 50,
    borderBottomColor: "#890011",
    borderBottomWidth: 1,
    borderTopColor: "#CD6062",
    borderTopWidth: 1
  }
});
