import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { HStack, VStack } from "native-base";
import { useSelector } from "react-redux";

const Badge = (props) => {
  return (
    <View>
      {/* Replace with image */}
      <Text>{props.label}</Text>
    </View>
  );
};

const UserInfo = () => {
  const userData = useSelector((state) => state.user.userData);
  console.log(userData);

  return (
    <HStack style={styles.container} justifyContent="space-between" alignItems="center">
      <VStack>
        <Text>Hi, {userData.userId}</Text>
        <Text>Welcome to LITTLEMORE INNOVATION LABS</Text>
      </VStack>
      <VStack>
        <Text>abc@xyz.com</Text>
        <Text>+91 98765 43210</Text>
      </VStack>
      <HStack space={5}>
        <Badge label="Thumb" />
        <Badge label="Smart Kid" />
        <Badge label="8th Std" />
      </HStack>
      <HStack>
        <Text>Promo/Updates</Text>
      </HStack>
    </HStack>
  );
};

export default UserInfo;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    padding: 30,
    borderBottomWidth: 1,
    borderBottomColor: "#DFDFDF"
  }
});
