import React from "react";
import { HStack, VStack, Text } from "native-base";
import HeadingWithCount from "./HeadingWithCount";

const ScheduleItem = (props) => {
  return (
    <HStack>
      <Text>{props.code.substring(0, 1)}</Text>
      <VStack>
        <Text>{props.subject}</Text>
        <Text>{props.code}</Text>
      </VStack>
    </HStack>
  );
};

const ScheduleToday = () => {
  return (
    <VStack>
      <HeadingWithCount title="Tdoay's Schedule" count={2} />
      <VStack>
        <ScheduleItem subject="ICSE 8 Std. Physics 1" code="PM10" />
        <ScheduleItem subject="ICSE 8 Std. Chemistry 1" code="CL10" />
      </VStack>
    </VStack>
  );
};

export default ScheduleToday;
