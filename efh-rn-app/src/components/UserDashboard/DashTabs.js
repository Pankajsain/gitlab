import React from "react";
import { SafeAreaView, View, Text, StyleSheet } from "react-native";

const DashTabs = () => {
  return (
    <SafeAreaView style={{ marginTop: 10 }}>
      <View style={{ flexDirection: "row", borderWidth: 1 }}>
        <View style={styles.examAlertMain}>
          <Text style={{ color: "#fff" }}>EXAM ALERT</Text>
        </View>
        <View style={{ padding: 5, flexDirection: "row" }}>
          <View style={{ padding: 12 }}>
            <Text>Lorem ipsum dolor sit amet, consctetur adipiscing elit</Text>
          </View>
          <View style={styles.examAlert}>
            <Text>Lorem ipsum dolor sit amet, consctetur adipiscing elit</Text>
          </View>
          <View style={styles.examAlert}>
            <Text>Lorem ipsum dolor sit amet, consctetur adipiscing</Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};
export default DashTabs;

const styles = StyleSheet.create({
  examAlert: {
    borderLeftWidth: 1,
    padding: 12
  },
  examAlertMain: {
    backgroundColor: "#000",
    borderWidth: 1,
    padding: 15
  }
});
