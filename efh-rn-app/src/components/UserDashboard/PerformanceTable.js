import { View, Text, StyleSheet, Button } from "react-native";
import React from "react";
import { VStack } from "native-base";
import HeadingWithCount from "./HeadingWithCount";

const PerformanceTable = () => {
  return (
    <VStack style={styles.secStyle}>
      <HeadingWithCount title="Exams Performance" count={5} />
      <Text>Performance</Text>
    </VStack>
  );
};

export default PerformanceTable;

const styles = StyleSheet.create({
  secStyle: {
    shadowColor: "lightgrey",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 1,
    shadowRadius: 5.46,
    elevation: 9
  },
  allOpt: {
    fontWeight: "bold"
  }
});
