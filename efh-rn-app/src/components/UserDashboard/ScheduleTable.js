import { View, Text, StyleSheet, Button } from "react-native";
import React, { useState, useEffect } from "react";
import { VStack, HStack } from "native-base";
import useMainNavigation from "~/helpers/hooks/screen-navigation/useMainNavigation";
import HeadingWithCount from "./HeadingWithCount";
import { DataTable, Provider as PaperProvider } from "react-native-paper";

const ScheduleTable = () => {
  const Schedule = [
    {
      status: "Downloaded",
      subject: "Physics 1",
      term: "Sem III",
      code: "PM101",
      date: "Wednesday, 23/06/2021"
    },
    {
      status: "Downloading",
      subject: "Chemistry 1",
      term: "Sem III",
      code: "CM202",
      date: "Thursday, 23/06/2021"
    },
    {
      status: "Not Started",
      subject: "Physics 2",
      term: "Sem IV",
      code: "PM202",
      date: "Friday, 23/06/2021"
    },
    {
      status: "Downloading",
      subject: "Chemistry 1",
      term: "Sem III",
      code: "CM202",
      date: "Thursday, 23/06/2021"
    },
    {
      status: "Not Started",
      subject: "Physics 2",
      term: "Sem IV",
      code: "PM202",
      date: "Friday, 23/06/2021"
    }
  ];

  const TableHeaders = [
    { label: "Code", type: "", flex: 0.65, key: "code" },
    { label: "Term", type: "", flex: 1, key: "term" },
    { label: "Subject", type: "", flex: 1.5, key: "subject" },
    { label: "Date & Time", type: "", flex: 2, key: "date" },
    { label: "Status", type: "", flex: 1, key: "status" },
    { label: "Action", type: "", flex: 0.75, key: "action" }
  ];

  const { navigateStartExam, navigateUserDashboard } = useMainNavigation();

  return (
    <VStack style={styles.secStyle}>
      <HeadingWithCount title="Exams Schedule" count={5} />
      <PaperProvider>
        <DataTable>
          <DataTable.Header>
            {TableHeaders.map((th, i) => (
              <DataTable.Title key={i} style={{ flex: th.flex }}>
                {th.label}
              </DataTable.Title>
            ))}
          </DataTable.Header>

          {Schedule.map((scheduleItem, i) => (
            <DataTable.Row key={i}>
              {TableHeaders.slice(0, -1).map((headerItem, j) => (
                <DataTable.Cell key={j} style={{ flex: headerItem.flex }}>
                  {scheduleItem[headerItem.key]}
                </DataTable.Cell>
              ))}
              <DataTable.Cell style={{ flex: TableHeaders[5].flex }}>
                <HStack justifyContent="center">
                  <Button title="Start" onPress={navigateStartExam} />
                </HStack>
              </DataTable.Cell>
            </DataTable.Row>
          ))}
        </DataTable>
      </PaperProvider>
    </VStack>
  );
};

export default ScheduleTable;

const styles = StyleSheet.create({
  secStyle: {
    shadowColor: "lightgrey",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 1,
    shadowRadius: 5.46,
    elevation: 9
  }
});
