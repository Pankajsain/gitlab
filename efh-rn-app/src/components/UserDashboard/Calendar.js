import React, { useState } from "react";
import CalendarPicker from "react-native-calendar-picker";

const Calendar = () => {
  const [selectedStartDate, setSelectedStartDate] = useState(null);
  const [selectedEndDate, setSelectedEndDate] = useState(null);
  const onDateChange = (date, type) => {
    //function to handle the date change
    if (type === "END_DATE") {
      setSelectedEndDate(date);
    } else {
      setSelectedEndDate(null);
      setSelectedStartDate(date);
    }
  };
  return (
    <CalendarPicker
      startFromMonday={true}
      minDate={new Date(2020, 1, 1)}
      maxDate={new Date(2050, 1, 1)}
      weekdays={["Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"]}
      months={[
        "January",
        "Febraury",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
      ]}
      previousTitle="Previous"
      previousTitleStyle={{ position: "absolute", left: 30, top: -4 }}
      nextTitleStyle={{ display: "none" }}
      //   customDayHeaderStyles={{backgroundColor:'red'}}
      scrollable={true}
      nextTitle="Next"
      nextTitleStyle={{ position: "absolute", right: 30, top: -4 }}
      width={360}
      height={380}
      todayBackgroundColor="#68a0cf"
      selectedDayColor="#68a0cf"
      selectedDayTextColor="#fff"
      scaleFactor={380}
      textStyle={{ fontFamily: "Cochin", color: "#000000" }}
      onDateChange={onDateChange}
    />
  );
};

export default Calendar;
