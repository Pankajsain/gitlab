import React from "react";
import { Text, StyleSheet, Image } from "react-native";
import { HStack, VStack, Button, Icon } from "native-base";
import useAuth from "~/helpers/hooks/useAuth";

const UserDashboardHeader = () => {
  const { logout } = useAuth();
  return (
    <HStack justifyContent="space-between" alignItems="center" style={styles.bar}>
      <HStack alignItems="center" space={5}>
        <Image
          style={{ width: 45, height: 45 }}
          source={require("~/assets/images/littlemore-logo-small.jpg")}
        />
        <Text style={{ padding: 12, fontSize: 20, fontWeight: "bold" }}>Dashboard</Text>
      </HStack>

      <HStack space={5} alignItems="center" style={{ backgroundColor: "transparent" }}>
        <Button transparent>
          <Text>Day/Night Toggle</Text>
        </Button>
        <HStack>
          <Text>Storage</Text>
          <Text>30 GB left</Text>
        </HStack>
        <HStack>
          <Text>Battery</Text>
          <Text>76%</Text>
        </HStack>
        <Button>
          <HStack>
            <Text>Alert</Text>
            <Text>4</Text>
          </HStack>
        </Button>
        <Button>
          <Text>Menu</Text>
        </Button>
        <Button transparent onPress={logout}>
          <Text>User Logout</Text>
        </Button>
      </HStack>
    </HStack>
  );
};
export default UserDashboardHeader;

const styles = StyleSheet.create({
  bar: {
    shadowColor: "lightgrey",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 1,
    shadowRadius: 4,
    paddingVertical: 5,
    paddingHorizontal: 20
  }
});
