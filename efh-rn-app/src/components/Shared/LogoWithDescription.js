import { Text } from "native-base";
import React from "react";
import { Image, StyleSheet, View } from "react-native";

const LogoWithDescription = ({ description }) => {
  return (
    <View style={styles.container}>
      <Image source={{ uri: "http://test.com/test" }} width={20} style={styles.logo} />
      <View>
        <Text style={styles.description}>{description}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  logo: {
    height: 100,
    width: 200,
    marginBottom: 20,
    backgroundColor: "lightgray"
  },
  description: {
    color: "gray"
  }
});

export default LogoWithDescription;
