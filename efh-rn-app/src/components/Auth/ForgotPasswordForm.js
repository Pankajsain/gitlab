import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { FormControl, Text } from "native-base";
import { useForm, Controller } from "react-hook-form";
import { useNavigation } from "@react-navigation/native";

import UITextInput from "../UI/Inputs/UITextInput";
import UIButton from "../UI/Inputs/UIButton";

const ForgotpasswordForm = () => {
  const { control, handleSubmit } = useForm();
  const navigation = useNavigation();

  const onSubmit = (data) => {
    console.log(data);
  };

  const loginHandler = () => {
    navigation.navigate("AuthSigninScreen");
  };

  return (
    <FormControl style={styles.form}>
      <Controller
        name="Username"
        control={control}
        rules={{ required: "Username required" }}
        render={({ field: { onChange, value }, fieldState: { error } }) => (
          <UITextInput
            value={value}
            label="Username"
            onChange={(val) => onChange(val)}
            style={styles.formInput}
            error={error}
          />
        )}
      />
      <Controller
        name="Email"
        control={control}
        render={({ field: { onChange, value }, fieldState: { error } }) => (
          <UITextInput
            value={"you_shall_not_pass@uni.com"}
            label="Email"
            style={styles.formInput}
            error={error}
            disabled={true}
          />
        )}
      />
      <View style={styles.actionsContainer}>
        <UIButton style={{ marginRight: 5 }} onClick={handleSubmit(onSubmit)} full>
          Forgot Password
        </UIButton>
        <UIButton style={{ marginLeft: 5 }} onClick={loginHandler} full>
          Login
        </UIButton>
      </View>
    </FormControl>
  );
};

const styles = StyleSheet.create({
  form: {
    width: "100%",
    maxWidth: 350,
    marginTop: 35
  },
  formInput: {
    marginBottom: 10
  },
  actionsContainer: {
    marginBottom: 10,
    display: "flex",
    flexDirection: "row"
  },
  errprMsg: {
    marginBottom: 10,
    color: "#ff0000"
  }
});

export default React.memo(ForgotpasswordForm);
