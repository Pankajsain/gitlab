import React from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";

import { useSelector } from "react-redux";
import { AuthNavigationScreens } from "~/shared/constants/NavigationScreens";
import UIButton from "~/components/UI/Inputs/UIButton";
import { Container } from "native-base";

import useAuth from "~/helpers/hooks/useAuth";

const ContinueAsUser = () => {
  const navigation = useNavigation();

  const userData = useSelector((state) => state.user.userData);
  console.log(userData);
  const { login } = useAuth();
  const continueSession = () => {
    login(userData);
  };

  const startNewSession = () => {
    navigation.navigate(AuthNavigationScreens.SignIn.name);
  };

  return (
    <View style={styles.container}>
      <View style={styles.userContainer}>
        <View style={styles.userBox}>
          <Text style={{ fontSize: 24, marginBottom: 10, fontWeight: "700", textAlign: "center" }}>
            Welcome back
          </Text>
          <View style={styles.userPhoto}></View>

          <UIButton full onClick={continueSession} style={{ width: "100%" }} dark>
            Continue as <Text style={{ fontWeight: "600" }}>{userData.fname.toUpperCase()}</Text>
          </UIButton>

          <TouchableOpacity onPress={startNewSession} style={{ marginTop: 10 }}>
            <Text
              style={{
                paddingHorizontal: 20,
                textDecorationLine: "underline",
                textAlign: "center"
              }}
            >
              No thanks, sign in as a different user.
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.bannerContainer}>
        <Text>Banner Image</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    flexDirection: "row"
  },
  userPhoto: {
    backgroundColor: "#f1f1f1",
    height: 300,
    marginBottom: 20,
    borderRadius: 5
  },
  userBox: {
    width: 350
  },
  userContainer: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  bannerContainer: {
    flexGrow: 1,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    backgroundColor: "lightgray",
    maxWidth: "40%"
  }
});

export default React.memo(ContinueAsUser);
