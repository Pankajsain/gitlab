import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { FormControl } from "native-base";
import { Controller, useForm } from "react-hook-form";
import { useNavigation } from "@react-navigation/native";

import UITextInput from "../UI/Inputs/UITextInput";
import UIButton from "../UI/Inputs/UIButton";
import UICheckBox from "../UI/Inputs/UICheckBox";
import UILinkButton from "../UI/Inputs/UILinkButton";
import useAuth from "~/helpers/hooks/useAuth";

const SigninForm = () => {
  const defaultValues = {
    username: "",
    password: ""
  };

  const { control, handleSubmit } = useForm({ defaultValues });

  const { login } = useAuth();

  const navigation = useNavigation();

  const forgotPasswordHandler = () => {
    navigation.navigate("AuthForgotPasswordScreen");
  };

  // const signUpHandler = () => {
  //   navigation.navigate("AuthSignupScreen");
  // };

  const onSubmit = (data) => {
    let userData = {
      userId: data.username,
      fname: data.username,
      lname: data.username
    };

    login(userData);
  };

  return (
    <FormControl style={styles.form}>
      <Controller
        name="username"
        defaultValue=""
        control={control}
        rules={{ required: "Username is required" }}
        render={({ field, fieldState: { error } }) => (
          <UITextInput label="Username" style={styles.formInput} {...{ ...field, error }} />
        )}
      />
      <Controller
        name="password"
        defaultValue=""
        control={control}
        rules={{ required: "Password is required" }}
        render={({ field, fieldState: { error } }) => (
          <UITextInput
            label="Password"
            style={styles.formInput}
            {...{ ...field, error }}
            isPassword
          />
        )}
      />
      <View style={styles.actionsContainer}>
        <UICheckBox label="Remember me" checked={false} />
        <UILinkButton onClick={forgotPasswordHandler}>Forgot Password?</UILinkButton>
      </View>
      <View style={styles.actionsContainer}>
        <UIButton style={{ marginRight: 5 }} onClick={handleSubmit(onSubmit)} full dark>
          Login
        </UIButton>
        {/* <UIButton style={{ marginLeft: 5 }} full bordered onClick={signUpHandler}>
          Sign up
        </UIButton> */}
      </View>
    </FormControl>
  );
};

const styles = StyleSheet.create({
  form: {
    width: "100%",
    maxWidth: 350,
    marginTop: 35
  },
  formInput: {
    marginBottom: 15
  },
  actionsContainer: {
    width: "100%",
    marginBottom: 15,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  }
});

export default React.memo(SigninForm);
