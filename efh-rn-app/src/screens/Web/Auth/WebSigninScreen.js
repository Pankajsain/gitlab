import React from "react";
import { StyleSheet, View } from "react-native";
import { Container, Text } from "native-base";

import SigninForm from "~/components/Auth/SigninForm";
import LogoWithDescription from "~/components/Shared/LogoWithDescription";

const WebSigninScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.formContainer}>
        <LogoWithDescription description="Welcome Back! Please Login to your account" />
        <SigninForm />
      </View>
      <View style={styles.bannerContainer}>
        <Text>Banner Image</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "row"
  },
  bannerContainer: {
    flexGrow: 1,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    backgroundColor: "lightgray",
    maxWidth: "40%"
  },
  formContainer: {
    flexGrow: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    padding: 10
  }
});

export default WebSigninScreen;
