import React, { useState } from "react";
import { Text, View, StyleSheet, SafeAreaView, Image, Button } from "react-native";
import { List, Icon, VStack, HStack } from "native-base";
import UserDashboardHeader from "~/components/UserDashboard/UserDashboardHeader";
import DashTabs from "~/components/UserDashboard/DashTabs";
import ScheduleTable from "~/components/UserDashboard/ScheduleTable";
import Calendar from "~/components/UserDashboard/Calendar";
import ListItemData from "~/components/UserDashboard/ListItemData";
import PerformanceTable from "~/components/UserDashboard/PerformanceTable";
import UserInfo from "~/components/UserDashboard/UserInfo";
import Sidebar from "~/components/UserDashboard/Sidebar";
import ScheduleToday from "~/components/UserDashboard/ScheduleToday";

const UserDashboardScreen = () => {
  return (
    <VStack style={styles.container}>
      <UserDashboardHeader />
      <HStack style={{ flex: 1 }}>
        <Sidebar />
        <VStack flexGrow="1" style={styles.rightContainer}>
          <DashTabs />
          <UserInfo />
          <HStack style={styles.examCalendarContainer}>
            <VStack flexGrow="1" space={10} style={{ backgroundColor: "#ffffff", padding: 30 }}>
              <ScheduleTable />
              <PerformanceTable />
            </VStack>
            <VStack style={styles.calendarScheduleCont}>
              <Calendar />
              <ScheduleToday />
            </VStack>
          </HStack>
        </VStack>
      </HStack>
    </VStack>
  );
};

export default UserDashboardScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  calendarScheduleCont: {
    paddingHorizontal: 30,
    backgroundColor: "#F7F9FB",
    borderColor: "#DFDFDF",
    borderRightWidth: 1,
    borderLeftWidth: 1
  },
  rightContainer: {
    padding: 30,
    backgroundColor: "#F5F7F9"
  },
  examCalendarContainer: {
    borderColor: "#DFDFDF",
    borderBottomWidth: 1
  }
});
