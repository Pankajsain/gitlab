// import React from "react";
// import { Container } from "native-base";
// import UserDashboardScreen from "../Web/UserDashboard/UserDashboardScreen";

// const DashboardScreen = () => {
//   return (
//     <Container>
//       <UserDashboardScreen />
//     </Container>
//   );
// };

// export default DashboardScreen;

import React from "react";
import {Button,Icon} from "native-base";
import {Pressable, View, Text ,SafeAreaView,Image,FlatList,StyleSheet} from 'react-native'
import withScreen from "~/helpers/hocs/withScreen";

// import UserInfo from "~/components/UserDashboard/UserInfo";
import UserDashboardScreen from "../Web/UserDashboard/UserDashboardScreen";
import { createDrawerNavigator } from '@react-navigation/drawer';
import Dashboard from "../MobileMenu/Dashboard";
import TariningCenter from "../MobileMenu/TariningCenter";
import ExemCenter from "../MobileMenu/ExemCenter";
import CameraScreen from "../MobileMenu/CameraScreen";



const Drawer = createDrawerNavigator();

const DashboardScreen = (props) => {
  return (
      <Drawer.Navigator>
        <Drawer.Screen options={{
         headerShown:true,
         headerTitle:()=>(
           <View style={{}}>
             <Image style={{width:100,height:25}} source={require('~/assets/images/littlemore-logo-large.jpg')}/>
           </View>
         ),
         headerRight:()=>(
           <View style={{marginRight:10}}>
             <Icon name={'person-circle'} />
           </View>
         )
        }} initialRouteName="Home"  name="Dashboard" component={Dashboard}  />
        <Drawer.Screen options={{headerShown:true}} name="Tarining Center" component={TariningCenter} />
        <Drawer.Screen options={{headerShown:true}} name="CameraScreen" component={CameraScreen} />
        <Drawer.Screen options={{headerShown:true}} name="Exem Center" component={ExemCenter} />
      </Drawer.Navigator>
  );
};

// export default DashboardScreen;
export default withScreen(DashboardScreen, UserDashboardScreen);


 
  