import React, {useEffect, useState, useRef} from 'react';
import {Button,Icon,ScrollView} from "native-base";
import {Pressable, View, Text ,SafeAreaView,Image,FlatList,StyleSheet} from 'react-native'
import CataSlider from '~/screens/MobileMenu/CataSlider'
import * as FaceDetector from 'expo-face-detector';
import { Camera } from "expo-camera";



const CameraScreen = ()=>{
    let cameraRef = useRef();
    const [hasPermission, setHasPermission] = useState(null);

    useEffect(() => {
        (async () => {
            const { status } = await Camera.requestPermissionsAsync();
            setHasPermission(status === "granted");
        })();
    }, []);

    if (hasPermission === null) {
        return <View />;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    async function detectFaces() {
        if(cameraRef !== null) {
            let photo = await cameraRef.takePictureAsync();
            console.log('photo uri', photo.uri);
            const options = { mode: FaceDetector.Constants.Mode.fast };
            console.log('detetcion', await FaceDetector.detectFacesAsync(photo.uri, options));
        }

    }

    return(
        <SafeAreaView>
            <View style={{marginHorizontal:15,paddingTop:10}}>
                
                <Camera
                    ref={ref => {
                        cameraRef = ref;
                    }}
                    style={{height: '80%', width:'100%'}}
                    type={Camera.Constants.Type.front}
                    // onFacesDetected={detectFaces}
                    onCameraReady={detectFaces}
                    faceDetectorSettings={{
                        mode: FaceDetector.Constants.Mode.fast,
                        detectLandmarks: FaceDetector.Constants.Landmarks.none,
                        runClassifications: FaceDetector.Constants.Classifications.none,
                        minDetectionInterval: 100,
                        tracking: true,
                    }}
                />
            </View>
     </SafeAreaView>
    )
}

export default CameraScreen;
