import React from 'react';
import {Text,FlatList,TouchableOpacity,View,Image} from 'react-native'


const DATA = [
    {
      id: '1',
      src:require('~/assets/images/Progressimg.jpg')
    },
    {
      id: '2',
      src:require('~/assets/images/Progressimg.jpg')
    },
    {
      id: '3',
      src:require('~/assets/images/Progressimg.jpg')
    },
    {
        id: '4',
        src:require('~/assets/images/Progressimg.jpg')
      },
      {
        id: '5',
        src:require('~/assets/images/Progressimg.jpg')
      },
  ];
const CataSlider=()=>{
    const renderItem = ({ item }) => (
        <View>
          <View style={{marginRight:9,marginTop:10}}>
             <Image style={{width:120,height:140,borderRadius:10}} 
             source={item.src}/>
          </View>
        </View>
         
        );
    return(
        <>
         <FlatList
            data={DATA}
            renderItem={renderItem}
            keyExtractor={(item,index) => index.toString()}
            horizontal
            maxToRenderPerBatch={2}
        />
        </>
    )
}
export default CataSlider;