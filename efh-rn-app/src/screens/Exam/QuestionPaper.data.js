import { AnswerType, MediaType, QuestionType } from "~/shared/constants/Types";

const QuestionPaperData = {
  id: "QP0001",
  center: "ABC College, Mumbai University",
  name: "Physics Paper II",
  marks: 100,
  schedule: {
    startTime: new Date(2021, 6, 28, 9, 0, 0, 0).toString(),
    endTime: new Date(2021, 6, 28, 12, 0, 0, 0).toString()
  },
  sections: [
    {
      id: "S0001",
      title: "S1",
      questions: [
        {
          id: "Q0001",
          marks: 3,
          markForLater: false,
          contentType: QuestionType.TEXT,
          content: {
            title: "Brainstroming was initially invented by ?",
            subTitle: "(select any one)"
          },
          answer: {
            contentType: AnswerType.TEXT_OPTIONS,
            content: {
              isMultiSelect: false,
              data: [
                { id: "A0001", selected: false, title: "Alex Osborn, an advertising executive" },
                {
                  id: "A0002",
                  selected: false,
                  title: "Eduard de Bono as part of his 6 Thinking Hats technique"
                },
                { id: "A0003", selected: false, title: "Bryan Mattimore, author of ideastomers" }
              ]
            }
          }
        },
        {
          id: "Q0002",
          marks: 3,
          markForLater: false,
          contentType: QuestionType.MEDIA,
          content: {
            type: MediaType.IMAGE,
            path: "",
            title: "Solve the problem given in the below diagram",
            subTitle: "(select any one)"
          },
          answer: {
            contentType: AnswerType.TEXT_OPTIONS,
            content: {
              isMultiSelect: false,
              data: [
                { id: "A0004", selected: false, title: "5m/s" },
                { id: "A0005", selected: false, title: "10m/s" },
                { id: "A0006", selected: false, title: "20m/s" }
              ]
            }
          }
        },
        {
          id: "Q0003",
          marks: 3,
          markForLater: false,
          contentType: QuestionType.TEXT,
          content: {
            title: "What is scattering angle?",
            subTitle: "(Type your answer below)"
          },
          answer: {
            contentType: AnswerType.TEXT_PARAGRAPH,
            content: { data: "" }
          }
        }
      ]
    }
  ]
};

export default QuestionPaperData;
