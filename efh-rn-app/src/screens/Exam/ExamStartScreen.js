import React, { useEffect } from "react";

import UIView from "~/components/UI/Layout/UIView";

import QuestionPaperHeader from "~/components/Exam/QuestionPaperHeader";
import QuestionPaper from "~/components/Exam/QuestionPaper";
import useExamSession from "~/helpers/hooks/useExamSession";

import questionPaperData from "./QuestionPaper.data";
import QuestionSections from "~/components/Exam/QuestionSections";
import { StyleSheet } from "react-native";
import Spacer from "~/components/UI/Common/Spacer";

const ExamStartScreen = () => {
  const { loadQuestionPaperData } = useExamSession();

  useEffect(() => {
    loadQuestionPaperData(questionPaperData);
  }, []);

  return (
    <UIView flexGrow={1} direction="column" width="100%">
      <UIView direction="column" width="100%" flexShrink={0}>
        <QuestionPaperHeader />
        <QuestionSections />
      </UIView>
      <UIView width="100%" height="100%" style={{ overflow: "auto" }}>
        <QuestionPaper />
      </UIView>
    </UIView>
  );
};

export default ExamStartScreen;
