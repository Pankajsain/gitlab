import React from "react";
import { StyleSheet } from "react-native";
import Contstants from "expo-constants";

import Spacer from "~/components/UI/Common/Spacer";

import UIButton from "~/components/UI/Inputs/UIButton";
import UnlockCode from "~/components/Exam/UnlockExam/UnlockCode";
import UIHeader from "~/components/UI/Text/UIHeader";
import UICamera from "~/components/UI/Native/UICamera";
import { Icon, Button, VStack, HStack, Text } from "native-base";
import { Ionicons } from "@expo/vector-icons";
import useExamNavigation from "~/helpers/hooks/screen-navigation/useExamNavigation";

import UnlockCamera from "~/components/Exam/UnlockExam/UnlockCamera";

const ExamUnlockScreen = () => {
  const exam = {
    title: "Physics Paper II".toUpperCase(),
    subTitle: "ABC College, Mumbai University".toUpperCase()
  };

  const { navigateExamStart } = useExamNavigation();

  const startExamHandler = () => {
    navigateExamStart();
  };

  return (
    <VStack style={styles.container}>
      <UIHeader title={exam.title} fontSize={28}>
        {exam.subTitle}
      </UIHeader>
      <Spacer size={10} />

      {/* <UICamera /> */}
      <UnlockCamera />

      <Spacer size={10} />
      <UnlockCode />
      <Spacer size={20} />

      <Button onClick={startExamHandler} style={styles.btnNext}>
        Next
        <Icon style={styles.icon} as={<Ionicons name="arrow-forward-outline" />} />
      </Button>
      <Spacer size={20} />

      <UIHeader title="Please note" sfontSize="12" fontSize="14">
        You can attend your exam even if you are not connected. You can upload can upload your
        answer sheet once you are connected to the internet.
      </UIHeader>
    </VStack>
  );
};

export default ExamUnlockScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  btnNext: {
    alignSelf: "center",
    paddingVertical: 0,
    borderRadius: 0,
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    flexWrap: "nowrap",
    width: 150
  },
  icon: {
    color: "white",
    fontSize: 14,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
});
