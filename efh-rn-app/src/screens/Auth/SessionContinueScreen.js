import React from "react";
import { StyleSheet } from "react-native";
import { View } from "native-base";
import ContinueAsUser from "~/components/Auth/ContinueAsUser";

const SessionContinueScreen = () => {
  return (
    <View style={styles.container}>
      <ContinueAsUser />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column"
  }
});

export default SessionContinueScreen;
