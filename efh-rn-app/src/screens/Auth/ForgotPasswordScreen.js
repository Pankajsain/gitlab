import React from "react";
import { StyleSheet } from "react-native";
import { View } from "native-base";

import ForgotPasswordForm from "~/components/Auth/ForgotPasswordForm";
import LogoWithDescription from "~/components/Shared/LogoWithDescription";

const ForgotPasswordScreen = () => {
  return (
    <View style={styles.container}>
      <LogoWithDescription description="Please enter username to recover your password" />
      <ForgotPasswordForm />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    padding: 10
  }
});

export default ForgotPasswordScreen;
