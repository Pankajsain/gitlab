import React from "react";
import { StyleSheet } from "react-native";
import { Container } from "native-base";

import withScreen from "~/helpers/hocs/withScreen";

import WebSigninScreen from "../Web/Auth/WebSigninScreen";

import SigninForm from "~/components/Auth/SigninForm";
import LogoWithDescription from "~/components/Shared/LogoWithDescription";

const SigninScreen = () => {
  return (
    <Container style={styles.container}>
      <LogoWithDescription description="Welcome Back! Please Login to your account" />
      <SigninForm />
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    padding: 10
  }
});

export default withScreen(SigninScreen, WebSigninScreen);
