import { useDispatch, useSelector } from "react-redux";
import {
  selectSingleAnswerOption,
  loadQuestionPaper,
  markQuestionForLater,
  updateTextAnswer
} from "~/store/slices/ExamSession.slice";

const useExamSession = () => {
  const dispatch = useDispatch();
  const questionPaper = useSelector((state) => state.examSession.questionPaper);
  const questionPaperDetails = useSelector((state) => {
    if (!state.examSession.questionPaper) return null;
    const { sections, ...details } = state.examSession.questionPaper;
    return details;
  });

  const loadQuestionPaperData = (questionPaperData) => {
    dispatch(loadQuestionPaper(questionPaperData));
  };

  const changeMarkForLaterStatus = (questionId, status) => {
    dispatch(markQuestionForLater({ questionId, status }));
  };

  const changeAnswerOption = (questionId, answerId) => {
    dispatch(selectSingleAnswerOption({ questionId, answerId }));
  };

  const changeAnswerText = (questionId, data) => {
    dispatch(updateTextAnswer({ questionId, data }));
  };

  return {
    questionPaper,
    questionPaperDetails,
    loadQuestionPaperData,
    changeMarkForLaterStatus,
    changeAnswerOption,
    changeAnswerText
  };
};

export default useExamSession;
