import { useDispatch, useSelector } from "react-redux";
import { signin, signout, reset } from "~/store/slices/User.slice";

const useAuth = () => {
  const dispatch = useDispatch();
  const userState = useSelector((store) => store.user);

  const login = (userData) => {
    console.log("dispatch login");
    dispatch(signin(userData));
    verifySession();
  };

  const logout = () => {
    dispatch(signout());
  };

  const resetSession = () => {
    dispatch(reset(false));
  };

  const verifySession = () => {
    console.log("start app");
    dispatch(reset(true));
  };

  return { userState, login, logout, resetSession, verifySession };
};

export default useAuth;
