import React from "react";
import { useNavigation } from "@react-navigation/core";
import { MainNavigationScreens } from "~/shared/constants/NavigationScreens";

function useMainNavigation() {
  const navigation = useNavigation();

  const navigateStartExam = () => {
    navigation.navigate(MainNavigationScreens.StartExam.name);
  };

  const navigateUserDashboard = () => {
    navigation.navigate(MainNavigationScreens.Dashboard.name);
  };

  return { navigateStartExam, navigateUserDashboard };
}

export default useMainNavigation;
