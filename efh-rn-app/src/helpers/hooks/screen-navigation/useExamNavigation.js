import React from "react";
import { useNavigation } from "@react-navigation/core";

import { ExamNavigationScreens } from "~/shared/constants/NavigationScreens";

const useExamNavigation = () => {
  const navigation = useNavigation();

  const navigateExamStart = () => {
    navigation.navigate(ExamNavigationScreens.ExamStart.name);
  };

  const navigateUnlockExam = () => {
    navigation.navigate(ExamNavigationScreens.Unlock.name);
  };

  return { navigateExamStart, navigateUnlockExam };
};

export default useExamNavigation;
