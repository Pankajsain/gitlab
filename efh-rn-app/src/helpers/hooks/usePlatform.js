const { Platform } = require("react-native");

const usePlatform = () => {
  const isWeb = Platform.OS === "web";
  const isIOS = Platform.OS === "ios";
  const isAndroid = Platform.OS === "android";
  const isMacOS = Platform.OS === "macos";
  const isWindows = Platform.OS === "windows";

  return { isWeb, isIOS, isAndroid, isMacOS, isWindows };
};

export default usePlatform;
