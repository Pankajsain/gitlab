import usePlatform from "../hooks/usePlatform";

const withScreen = (Screen, WebScreen) => {
  const { isWeb } = usePlatform();
  if (isWeb) return WebScreen;
  return Screen;
};

export default withScreen;
